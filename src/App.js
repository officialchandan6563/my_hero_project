import React from "react";
import Header from "./Components/Header/header"
import Home from "./Components/Homes/home"

function App() {
  return (
    <div>
     <Header/>
     <Home/>
    </div>
  );
}

export default App;
