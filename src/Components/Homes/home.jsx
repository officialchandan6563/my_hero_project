import React from 'react'
import './homeStyle.css'
import Rocket from './../../Assets/images/hero-carousel-3.svg'

function home() {
  return (
    <div className='home_main_block'>
        <div className="home_sub_block">
            <div className="home_sub_block_banner">
                <img src={Rocket} alt="img" style={{width:'100%', height:'100%',}}/>
            </div>
            <div className="home_sub_block_content">
              <h3>Welcome to <span>HeroBiz</span></h3>
              <p>Et voluptate esse accusantium accusamus natus reiciendis quidem voluptates similique aut.</p>
              <button className = "content_btn">Get Started</button>
            </div>
        </div>
    </div>
  )
}

export default home
