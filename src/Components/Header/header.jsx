import React from 'react'
import './headerStyle.css'

function header() {
  return (
    <div className='header_main_block'>
      <div className="header_sub_block">
        <div className="header_sub_block_logo">
            <h3>HeroBiz.</h3>
        </div>
        <div className="header_sub_block_navbar">
            <ul className='nav_links'>
                <li>Home <span><i className="fa fa-angle-down"></i></span></li>
                <li>About</li>
                <li>Services</li>
                <li>Portfolio</li>
                <li>Team</li>
                <li>Blog</li>
                <li>Mega Menu</li>
                <li>Drop Down</li>
                <li>Contact</li>
            </ul>
        </div>
        <div className="header_sub_block_button">
            <button>Get Started</button>
        </div>
      </div>
    </div>
  )
}

export default header
